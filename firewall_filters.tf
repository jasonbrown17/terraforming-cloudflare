#----- firewall_filters.tf 

resource "cloudflare_filter" "example_geoip_filter" {
  zone_id     = data.cloudflare_zone.example_domain.id
  description = "GeoIP Blocks"
  expression  = "(ip.geoip.country eq \"RU\")"
}

resource "cloudflare_filter" "example_xmlrpc_filter" {
  zone_id     = data.cloudflare_zone.example_domain.id
  description = "Block XML RPC"
  expression  = "(http.request.uri.path contains \"/xmlrpc.php\")"
}

resource "cloudflare_filter" "example_direct_comments_filter" {
  zone_id     = data.cloudflare_zone.example_domain.id
  description = "Block Direct Requests To WP Comments"
  expression  = "(http.request.uri.path eq \"/wp-comments-post.php\" and http.request.method eq \"POST\" and http.referer ne \"example.com\")"
}