#----- mx_records.tf 

resource "cloudflare_record" "example_mx_record" {
  zone_id  = data.cloudflare_zone.example_domain.id
  name     = "example.com"
  type     = "MX"
  ttl      = "300"
  value    = "outlook.com"
  priority = "0"
}