#----- firewall_rules.tf 

resource "cloudflare_firewall_rule" "example_geoip_rule" {
  zone_id     = data.cloudflare_zone.example_domain.id
  description = "GeoIP Blocks"
  filter_id   = cloudflare_filter.example_geoip_filter.id
  action      = "block"
  priority    = "1"
}

resource "cloudflare_firewall_rule" "example_xmlrpc_rule" {
  zone_id     = data.cloudflare_zone.example_domain.id
  description = "Block XMLRPC"
  filter_id   = cloudflare_filter.example_xmlrpc_filter.id
  action      = "block"
  priority    = "2"
}

resource "cloudflare_firewall_rule" "example_comments_rule" {
  zone_id     = data.cloudflare_zone.example_domain.id
  description = "Block Direct Requests To WP Comments"
  filter_id   = cloudflare_filter.example_direct_comments_filter.id
  action      = "block"
  priority    = "3"
}