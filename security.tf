#-----security.tf

resource "cloudflare_zone_settings_override" "example_security" {
  zone_id = data.cloudflare_zone.example_domain.id
  settings {
    brotli                   = "on"
    ssl                      = "full"
    waf                      = "on"
    automatic_https_rewrites = "on"
    min_tls_version          = "1.2"
    hotlink_protection       = "on"
    http2                    = "on"
    http3                    = "on"
    mirage                   = "on"
    webp                     = "on"
    security_header {
      enabled = true
      nosniff = true
    }
  }
}