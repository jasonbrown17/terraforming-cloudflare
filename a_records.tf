#----- a_records.tf 

resource "cloudflare_record" "example_a_record" {
  zone_id = data.cloudflare_zone.example_domain.id
  name    = "example.com"
  type    = "A"
  ttl     = "1"
  value   = "1.1.1.1"
  proxied = true
}