#----- cname_records.tf 

resource "cloudflare_record" "example_www_cname_record" {
  zone_id = data.cloudflare_zone.example_domain.id
  name    = "www"
  type    = "CNAME"
  value   = "example.com"
  ttl     = "1"
  proxied = true
}